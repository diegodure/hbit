// angular.module('indexEcommerce',['angularModalService', '720kb.datepicker','moment-picker'])
angular.module('indexEcommerce',['angularModalService','slickCarousel'])

.factory("flash", function($rootScope) {

  return {

    pop: function(message) {
      switch(message.type) {
        case 'success':
          toastr.success(message.body, message.title);
          break;
        case 'info':
          toastr.info(message.body, message.title);
          break;
        case 'warning':
          toastr.warning(message.body, message.title);
          break;
        case 'error':
          toastr.error(message.body, message.title);
          break;
      }
    }
  };
})

.directive('numberConverter', function() {
  return {
    priority: 1,
    restrict: 'A',
    require: 'ngModel',
    link: function(scope, element, attr, ngModel) {
      function toModel(value) {
        return "" + value; // convert to string
      }

      function toView(value) {
        return parseInt(value); // convert to number
      }

      ngModel.$formatters.push(toView);
      ngModel.$parsers.push(toModel);
    }
  };
})

.controller('indexEcommerceCtrl', function($scope, $http, ModalService,flash,$window){
	angular.element(document).ready(function () {
	    
	    $scope.loadCategories();
      $scope.loadProducts();
      $scope.shoppingCartList = [];
	    
	});

	$scope.loadProducts = function(){
		$http.get('models/selectProducts.php').success(function(data){
      console.log(data)
      $scope.productsList = data;
    })
	}

  $scope.loadCategories = function(){
    // Validar dependiendo de la resolucion de la pantalla
    $scope.slideToShow = 1;
    $http.get('models/selectCategories.php').success(function(data){
      console.log(data)
      $scope.categoriesList = data;
      $scope.slickConfig = {
          enabled: true,
          autoplay: true,
          draggable: true,
          autoplaySpeed: 3000,
          dots: false,
          method: {},
          event: {
              beforeChange: function (event, slick, currentSlide, nextSlide) {
              },
              afterChange: function (event, slick, currentSlide, nextSlide) {
              }
          }
      };

      $scope.numberLoaded = true;
      $scope.numberUpdate();
    });
  };

  $scope.numberUpdate = function(){
      $scope.numberLoaded = false; // disable slick

      //number update

      $scope.numberLoaded = true; // enable slick
  };

  $scope.showSingleProduct = function(product){
    console.log(product)
    //alert(cliente);
    $scope.singleProduct = product;
  }

  $scope.addProductToCart = function(product){
    let existProduct = false;
    for (var i = 0; i < $scope.shoppingCartList.length; i++) {
      if($scope.shoppingCartList[i].idProducts == product.idProducts){
        existProduct = true;
        $scope.shoppingCartList[i].Stock++;
        break;
      }
    }
    if(!existProduct){
      let productToAdd = product;
      productToAdd.Stock = 1;
      $scope.shoppingCartList.push(product);
    }
  }

  $scope.addRestProductInCart = function(cartList,sumRestChange){
    if(sumRestChange == 'rest'){
      if(cartList.Stock == 1){
        $scope.msgTitle = 'Atención!';
        $scope.msgBody  = 'La cantidad mínima es 1';
        $scope.msgType  = 'warning';
        flash.pop({title: $scope.msgTitle, body: $scope.msgBody, type: $scope.msgType});
      }else{
        cartList.Stock--;
      }
    }else if(sumRestChange == 'sum'){
      cartList.Stock++;
    }else if(sumRestChange == 'change'){
      cartList.Stock = angular.element($("#amountProductCart-"+cartList.idProducts)).val();
      if(cartList.Stock == 0 || cartList.Stock == "0"){
        cartList.Stock = 1;
      }
    }
  }

  $scope.sendByWhatsapp = function(){
    console.log($scope.shoppingCartList)
    var listaFormateada = ''; // Formato de la lista de productos
        
        // Formatear la lista de productos
        angular.forEach($scope.shoppingCartList, function(producto) {
            listaFormateada += producto.Name + ' - Cantidad: ' + producto.Stock + '\n';
            // Puedes agregar más información del producto si es necesario
        });

        // Ejemplo de acción: copiar la lista al portapapeles
        // window.navigator.clipboard.writeText(listaFormateada)
        // Realizar una solicitud POST al backend PHP para enviar el mensaje de WhatsApp
        // $http.post('models/sendProductCartListByWhatsapp.php', { mensaje: listaFormateada })
        //     .then(function(response) {
        //         alert('Mensaje enviado por WhatsApp');
        //     })
        //     .catch(function(error) {
        //         alert('Error al enviar el mensaje por WhatsApp');
        //     });

        // Construir la URL de WhatsApp
        var numeroTelefono = '+5991454527'; // Reemplaza con tu número de teléfono
        var url = 'https://api.whatsapp.com/send?phone=' + encodeURIComponent(numeroTelefono) + '&text=' + encodeURIComponent(listaFormateada);

        // Redirigir al usuario a la URL de WhatsApp
        $window.open(url, '_blank');
  }

})

