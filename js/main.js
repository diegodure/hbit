var categoriesList = [
	{id:1,title:"titulo1"},
	{id:2,title:"titulo2"},
	{id:3,title:"titulo3"},
	{id:4,title:"titulo4"},
	{id:5,title:"titulo5"},
	{id:6,title:"titulo6"},
	{id:7,title:"titulo7"},
	{id:8,title:"titulo8"},
	{id:9,title:"titulo9"},
	{id:10,title:"titulo10"}
]

function loadCategories(){
	var html = "";

	for(var i = 0; i < categoriesList.length;i++){
		html += "<div class='item colorHbitHoverBorder'>"; 
		html += "<a href='' class='text-decoration-none text-inherit'>";
	    html += "<div class='card card-product mb-lg-4'>";
	    html += "<div class='card-body text-center py-8'>";
	    html += "<img src=featuredCategories/imagen"+i+".jpg alt='' class='mb-3 img-fluid imgPopularProducts'>";   
	    html += "<div class='text-truncate'>"+categoriesList[i].title+"</div>";
	    html += "</div>";
	    html += "</div>";
	    html += "</a>";
	    html += "</div>";
	}

    jQuery("#categoriesSection").html(html);

    $(".category-slider.slick-initialized.slick-slider").slick('destroy');
    $(".category-slider").slick('reinit');
}

var popularProductsList = [
	{id:1,title:"titulo1",description:"descripcion1",category:"categoria1",currentPrice:"20000",offerPrice:"40000"},
	{id:2,title:"titulo2",description:"descripcion2",category:"categoria2",currentPrice:"50000",offerPrice:"40000"},
	{id:3,title:"titulo3",description:"descripcion3",category:"categoria3",currentPrice:"30000",offerPrice:"40000"},
	{id:4,title:"titulo4",description:"descripcion4",category:"categoria4",currentPrice:"70000",offerPrice:"40000"},
	{id:5,title:"titulo5",description:"descripcion5",category:"categoria5",currentPrice:"10000",offerPrice:""},
	{id:6,title:"titulo6",description:"descripcion6",category:"categoria6",currentPrice:"90000",offerPrice:"40000"},
	{id:7,title:"titulo7",description:"descripcion7",category:"categoria7",currentPrice:"66000",offerPrice:""},
	{id:8,title:"titulo8",description:"descripcion8",category:"categoria8",currentPrice:"78900",offerPrice:"40000"},
	{id:9,title:"titulo9",description:"descripcion9",category:"categoria9",currentPrice:"105000",offerPrice:""},
	{id:10,title:"titulo10",description:"descripcion10",category:"categoria10",currentPrice:"55500",offerPrice:"40000"}
]

function loadPopularProducts(){
	var html = "";
	var colStyle = "";
	var valueColOffert = "00000";
	for (var i = 0; i < popularProductsList.length; i++) {
		html += "<div class='col'>";
		html += "<div class='card card-product colorHbitHoverBorder'>";
		html += "<div class='card-body'>";
		html += "<div class='text-center position-relative'>";
		if(popularProductsList[i].offerPrice){
			html += "<div class='position-absolute top-0 start-0'>";
			html += "<span class='badge bg-danger'>Sale</span>";
			//<span class="badge bg-success">14%</span>
			html += "</div>";
		}
		html += "<a href=''>";
		html += "<img src=featuredCategories/imagen"+i+".jpg alt='' class='mb-3 img-fluid imgPopularProducts'>";
		html += "</a>";
		html += "<div class='card-product-action'>";
		html += "<a href='#!'' class='btn-action' data-bs-toggle='modal' data-bs-target='#quickViewModal' onclick=showSingleProduct("+popularProductsList[i].id+")>";
		html += "<i class='bi bi-eye' data-bs-toggle='tooltip' data-bs-html='true' title='Ver'>";
		html += "</i></a>";
		// html += "<a href='#!' class='btn-action' data-bs-toggle='tooltip' data-bs-html='true' title='Favoritos'>";
		// html += "<i class='bi bi-heart'></i></a>";
		html += "</div>";
		html += "</div>";
		html += "<div class='text-small mb-1'>";
		html += "<a href='#!' class='text-decoration-none text-muted'>";
		html += "<small>"+popularProductsList[i].title+"</small>";
		html += "</a></div>";
		html += "<h2 class='fs-6'>";
		html += "<a href='' class='text-inherit text-decoration-none colorHbitHover'>";
		html += popularProductsList[i].description+"</a></h2>";
		html += "<div></div>";
		html += "<div class='d-flex justify-content-between align-items-center mt-3'>";
		html += "<div><span class='text-dark'>"+popularProductsList[i].currentPrice+" gs.</span>";
		if(popularProductsList[i].offerPrice){
			colStyle = "";
			valueColOffert = popularProductsList[i].offerPrice;
		}else{
			colStyle = "visibility: hidden;";
			valueColOffert = "00000";
		}
		html += "<span class='text-decoration-line-through text-muted' style='"+colStyle+"'>"+valueColOffert+" gs</span>";
		html += "</div>";
		html += "<div onmouseover='divToAddProductInCard(this)' onmouseleave='divToAddProductInCard(this,true)'>";
		html += "<a href='#!' onclick=addProductToCart("+popularProductsList[i].id+") class='btn btn-primary btn-sm colorHbitBackground colorHbitHoverBorder'>";
		html += "<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-plus'>";
		html += "<line x1='12' y1='5' x2='12' y2='19'></line>";
		html += "<line x1='5' y1='12' x2='19' y2='12'></line>";
		html += "</svg> Add</a></div>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
	}
	jQuery("#colPopularProducts").html(html)
}

function showSingleProduct(id){
	var productInstance;
	for(var i = 0; i < popularProductsList.length; i++){
		if(popularProductsList[i].id == id){
			productInstance = popularProductsList[i];
			break;
		}
	}
	var html = "";

	html += "<div class='col-lg-6'>";
	html += "<div class='product productModal'>";//id='productModal'
	html += "<div class='zoom' onmousemove='zoom(event)' style='background-image: url(featuredCategories/imagen"+i+".jpg);'>";
	html += "<img src=featuredCategories/imagen"+i+".jpg alt=''>";
	html += "</div>";
	// html += "<div>";
	// html += "<div class='zoom' onmousemove='zoom(event)' style='background-image: url(product-single-img-2.jpg);'>";
	// html += "<img src='product-single-img-2.jpg' alt=''>";
	// html += "</div>";
	// html += "</div>";
	// html += "<div>";
	// html += "<div class='zoom' onmousemove='zoom(event)' style='background-image: url(product-single-img-3.jpg);'>";
	// html += "<img src='product-single-img-3.jpg' alt=''>";
 //    html += "</div>";
 //    html += "</div>";
    // html += "<div>";
    // html += "<div class='zoom' onmousemove='zoom(event)' style='background-image: url(product-single-img-4.jpg);'>";
    // html += "<img src='product-single-img-4.jpg' alt=''>";     
    // html += "</div>";
    // html += "</div>";
    html += "</div>";
    html += "<div class='product-tools'>";
    html += "<div class='thumbnails row g-3' id='productModalThumbnails'>";
    html += "<div class='col-3' class='tns-nav-active'>";
    html += "<div class='thumbnails-img'>";
    html += "<img src=featuredCategories/imagen"+i+".jpg alt=''>";
    html += "</div>";
    html += "</div>";
   //  html += "<div class='col-3'>";
   //  html += "<div class='thumbnails-img' >";
   //  html += "<img src='product-single-img-2.jpg' alt=''>";
  	// html += "</div>";
  	// html += "</div>";
  	// html += "<div class='col-3'>";
  	// html += "<div class='thumbnails-img'>";
  	// html += "<img src='product-single-img-3.jpg' alt=''>";
   //  html += "</div>";
   //  html += "</div>";
    // html += "<div class='col-3'>";
    // html += "<div class='thumbnails-img'>";
    // html += "<img src='product-single-img-4.jpg' alt=''>";
    // html += "</div>";
    // html += "</div>";
    html += "</div>";
    html += "</div>";
    html += "</div>";
    html += "<div class='col-lg-6'>";
    html += "<div class='ps-lg-8 mt-6 mt-lg-0'>";
    html += "<a href='#!' class='mb-4 d-block'>"+productInstance.category+"</a>";
    html += "<h2 class='mb-1 h1'>"+productInstance.title+"</h2>";   
   	html += "<div class='mb-4'>";
    html += "<small class='text-warning'>"; 

    html += "</small>";
    html += "</div>";     
    html += "<div class='fs-4'>";
    html += "<span class='fw-bold text-dark'>"+productInstance.currentPrice+" gs.</span>";
    if(productInstance.offerPrice){
    	html += "<span class='text-decoration-line-through text-muted'>"+productInstance.offerPrice+" gs.</span>";
    }
    html += "</div>";
    html += "<hr class='my-6'>";
    html += "<div>";
    html += "<div class='input-group input-spinner'>";
    html += "<input type='button' value='-' class='button-minus  btn  btn-sm' data-field='quantity'>"
    html += "<input type='number' step='1' max='10' value='1' name='quantity' class='quantity-field form-control-sm form-input'>";
    html += "<input type='button' value='+' class='button-plus btn btn-sm' data-field='quantity'>";
    html += "</div>";
    html += "</div>";
    html += "<div class='mt-3 row justify-content-start g-2 align-items-center'>";
    html += "<div class='col-lg-4 col-md-5 col-6 d-grid'>";
    html += "<button type='button' class='btn btn-primary colorHbitBackground colorHbitHoverBorder'>";
    html += "<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-plus'><line x1='12' y1='5' x2='12' y2='19'></line><line x1='5' y1='12' x2='19' y2='12'></line></svg>";

    html += "Add</button>";
    html += "</div>";
    html += "<div class='col-md-4 col-5'>";

    html += "</div>";
    html += "</div>";
    html += "<hr class='my-6'>";
    html += "<div>";
    html += "<table class='table table-borderless'>";
    html += "<tbody>";
    html += "<tr>";
    html += "<td>Código del producto:</td>";
    html += "<td>"+productInstance.id+"</td>";
    html += "</tr>";
    html += "<tr>";
    html += "<td>Disponibilidad:</td>";
    html += "<td>En Stock</td>";
    html += "</tr>";
    html += "<tr>";
    html += "<td>Categoría:</td>";
    html += "<td>Categoría1</td>";
    html += "</tr>";           
    html += "</tbody>";
    html += "</table>";
    html += "</div>";
    html += "</div>";
    html += "</div>";  

    jQuery("#rowSingleModal").html(html)            
}

var productListInCart = [];

function getProductsInCart(){
	var array = localStorage.getItem('productListInCartLocalStorage');
	// Se parsea para poder ser usado en js con JSON.parse :)
	array = JSON.parse(array);
	if(array.length > 0){
		for(var i = 0; i < array.length; i++){
			addProductToCart(array[i].id,array[i].quantity,array[i].currentPrice)
		}
	}
}

function addRestProductInCart(id,sumRest){
	var actualQuantity = jQuery("#productList-"+id+" .quantity-field").val();
	var actualSubTotal = jQuery("#productList-"+id+" .productSubTotal").text();
	var productPrice = 0;
	var productListInCartInstance = "";
	actualSubTotal = actualSubTotal.split("gs");
	if(sumRest == 'sum'){
		actualQuantity = parseInt(actualQuantity) + 1;
		jQuery("#productList-"+id+" .quantity-field").val(actualQuantity);
		productListInCartInstance = checkProductListInCart(id);
		productPrice = productListInCartInstance.currentPrice / productListInCartInstance.quantity; 
		actualSubTotal = productPrice*actualQuantity;
		jQuery("#productList-"+id+" .productSubTotal").text(actualSubTotal+"gs");
	}else if(sumRest == 'rest'){
		if(actualQuantity == 1 || actualQuantity == '1'){
			alert("La cantidad mínima es 1");
			return;
		}else{
			actualQuantity = parseInt(actualQuantity) - 1;
			jQuery("#productList-"+id+" .quantity-field").val(actualQuantity);
			productListInCartInstance = checkProductListInCart(id);
			productPrice = productListInCartInstance.currentPrice / productListInCartInstance.quantity;
			actualSubTotal = productPrice*actualQuantity;
			jQuery("#productList-"+id+" .productSubTotal").text(actualSubTotal+"gs");
		}
	}else{
		if(actualQuantity == 0 || actualQuantity == '0'){
			actualQuantity = 1;
			jQuery("#productList-"+id+" .quantity-field").val(1);
			productListInCartInstance = checkProductListInCart(id);
			productPrice = productListInCartInstance.currentPrice / 1;
			actualSubTotal = productPrice*actualQuantity;
			jQuery("#productList-"+id+" .productSubTotal").text(actualSubTotal+"gs");
		}else{
			actualQuantity = parseInt(actualQuantity);
			productListInCartInstance = checkProductListInCart(id);
			productPrice = productListInCartInstance.currentPrice / productListInCartInstance.quantity;
			actualSubTotal = productPrice*actualQuantity;
			jQuery("#productList-"+id+" .productSubTotal").text(actualSubTotal+"gs");
		}
	}
	for(var i = 0; i < productListInCart.length; i++){
		if(productListInCart[i].id == id){
			productListInCart[i].quantity = actualQuantity;
			productListInCart[i].currentPrice = actualSubTotal;
			break;
		}
	}
	localStorage.setItem('productListInCartLocalStorage', JSON.stringify(productListInCart));
}

function checkProductListInCart(id){
	for(var i = 0; i < productListInCart.length; i++){
		if(productListInCart[i].id == id){
			return productListInCart[i];
		}
	}
}

function checkPopularProductList(id){
	for(var i = 0; i < popularProductsList.length; i++){
		if(popularProductsList[i].id == id){
			return popularProductsList[i];
		}
	}
}

function addProductToCart(id,quantityFromLocalStorage,currentPriceFromLocalStorage){
	jQuery(".thereNotProductsInCardLabel").css("display","none");

	var productInstance;
	var indice;
	var productPrice = 0;

	productInstance = checkPopularProductList(id);

	if(!productInstance.quantity){
		productInstance.quantity = 1;
	}
	
	productInstance.currentPrice = parseInt(productInstance.currentPrice);
	if(quantityFromLocalStorage){
		productInstance.quantity = quantityFromLocalStorage;
	}
	if(currentPriceFromLocalStorage){
		productInstance.currentPrice = currentPriceFromLocalStorage;
	}


	for(var i = 0; i < productListInCart.length; i++){
		if(productListInCart[i].id == productInstance.id){
			var actualQuantity = jQuery("#productList-"+productInstance.id+" .quantity-field").val();
			actualQuantity = parseInt(actualQuantity) + 1;
			jQuery("#productList-"+productInstance.id+" .quantity-field").val(actualQuantity);
			productPrice = productInstance.currentPrice / productInstance.quantity;
			actualSubTotal = productPrice*actualQuantity;
			productListInCart[i].currentPrice = actualSubTotal;
			jQuery("#productList-"+id+" .productSubTotal").text(actualSubTotal+"gs");
			productListInCart[i].quantity = actualQuantity;

			localStorage.setItem('productListInCartLocalStorage', JSON.stringify(productListInCart));
			return;
		}
	}

	var html = "";

	html += "<li class='list-group-item py-3 ps-0 border-top' id=productList-"+productInstance.id+">";
	html += "<div class='row align-items-center'>";
	html += "<div class='col-6 col-md-6 col-lg-7'>";
	html += "<div class='d-flex'>";
	html += "<img src=featuredCategories/imagen"+productInstance.id+".jpg alt='Ecommerce' class='icon-shape icon-xxl'>";
	html += "<div class='ms-3'>";
	html += "<a href='' class='text-inherit'>";
	html += "<h6 class='mb-0'>"+productInstance.title+"</h6>";
	html += "</a>";
    html += "<span><small class='text-muted'>"+productInstance.description+"</small></span>";        
    html += "<div class='mt-2 small lh-1' onclick=deleteProductFromCart("+productInstance.id+")>";
    html += "<a class='text-decoration-none text-inherit' style='cursor:pointer;'>";
    html += "<span class='me-1 align-text-bottom'>";
    html += "<svg style='color:red !important' xmlns='http://www.w3.org/2000/svg' width='14' height='14' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-trash-2 text-success'>";
    html += "<polyline points='3 6 5 6 21 6'></polyline>";
    html += "<path d='M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2'>";
    html += "</path>";
    html += "<line x1='10' y1='11' x2='10' y2='17'></line>";
    html += "<line x1='14' y1='11' x2='14' y2='17'>";
    html += "</line>";
    html += "</svg>";
    html += "</span><span class='text-muted'>Quitar</span></a></div></div>";                  
    html += "</div></div>";                  
    html += "<div class='col-4 col-md-3 col-lg-3'>";
    html += "<div class='input-group input-spinner'>";
    html += "<input type='button' value='-' class='button-minus  btn  btn-sm' data-field='quantity' onclick=addRestProductInCart("+productInstance.id+",'rest')>";
    html += "<input type='number' step='1' max='10' min='1' value='"+productInstance.quantity+"' name='cantidad' onkeypress='return event.charCode >= 48 && event.charCode <= 57' class='quantity-field form-control-sm form-input' onchange=addRestProductInCart("+productInstance.id+",'change')>";
    html += "<input type='button' value='+' class='button-plus btn btn-sm' data-field='cantidad' onclick=addRestProductInCart("+productInstance.id+",'sum')>";
    html += "</div></div>";        
    html += "<div class='col-2 text-lg-end text-start text-md-end col-md-2'>";
    html += "<span class='fw-bold productSubTotal'>"+productInstance.currentPrice+"gs</span>";     
    html += "</div></div></li>";

    jQuery("#cartContent").append(html);

    productListInCart.push(productInstance);
    jQuery(".counterProductInCart").text(productListInCart.length);
    localStorage.setItem('productListInCartLocalStorage', JSON.stringify(productListInCart));
}

function deleteProductFromCart(id){
	jQuery("#productList-"+id).remove();
	if(jQuery("#cartContent li").length == '0' || jQuery("#cartContent li").length == 0){
		jQuery(".thereNotProductsInCardLabel").css("display","");
	}
	for(var i = 0; i < productListInCart.length; i++){
		if(productListInCart[i].id == id){
			productListInCart.splice(i,1);
			break;
		}
	}
	jQuery(".counterProductInCart").text(productListInCart.length)
	localStorage.setItem('productListInCartLocalStorage', JSON.stringify(productListInCart));
}

function divToAddProductInCard(div,isLeave){
	if(isLeave){
		div.querySelector("a").style.background = '#170f3e'
	}else{
		div.querySelector("a").style.background = '#0e99ff'
	}
	
}
var temporalOffertsList = [
	{id:1,title:"titulo1",description:"descripcion1",category:"categoria1",currentPrice:50000,offerPrice:40000},
	{id:2,title:"titulo2",description:"descripcion2",category:"categoria2",currentPrice:60000,offerPrice:50000},
	{id:1,title:"titulo3",description:"descripcion3",category:"categoria3",currentPrice:30000,offerPrice:20000},
	{id:1,title:"titulo4",description:"descripcion4",category:"categoria4",currentPrice:100000,offerPrice:85000}
]
function temporalOferts(){
	var html = "";
	for(var i = 0; i < temporalOffertsList.length; i++){
		html += "<div class='col'>";
		html += "<div class='card card-product'>";
		html += "<div class='card-body'>";
		html += "<div class='text-center  position-relative'> <a href=''>";
		html += "<img src=featuredCategories/imagen"+i+".jpg  alt='' class='mb-3 img-fluid'></a>";
		html += "<div class='card-product-action'>";
		html += "<a href='#!' class='btn-action' data-bs-toggle='modal' data-bs-target='#quickViewModal' onclick=showSingleProduct("+temporalOffertsList[i].id+")>";
		html += "<i class='bi bi-eye' data-bs-toggle='tooltip' data-bs-html='true' title='Quick View'>";
		html += "</i></a>";
		// html += "<a href='#!' class='btn-action' data-bs-toggle='tooltip' data-bs-html='true' title='Wishlist'>";
		// html += "<i class='bi bi-heart'></i></a>";
		html += "</div>";
		html += "</div>";
		html += "<div class='text-small mb-1'><a href='#!'' class='text-decoration-none text-muted'>";
		html += "<small>"+temporalOffertsList[i].title+"";
		html += "</small></a></div>";
		html += "<h2 class='fs-6'>";
		html += "<a href='#!' class='text-inherit text-decoration-none'>";
		html += temporalOffertsList[i].description+"</a></h2>";
		html += "<div class='d-flex justify-content-between align-items-center mt-3'>";
		html += "<div><span class='text-dark'>"+temporalOffertsList[i].currentPrice+" gs.</span>";
		html += "<span class='text-decoration-line-through text-muted'>"+temporalOffertsList[i].offerPrice+" gs</span>";
		html += "</div><div>";
		html += "<small class='text-warning'>";
		html += "</span>";
		html += "</div></div>";
		html += "<div class='d-grid mt-2' onmouseover='divToAddProductInCard(this)' onmouseleave='divToAddProductInCard(this,true)'>";
		html += "<a href='#!' class='btn btn-primary colorHbitBackground colorHbitHoverBorder' onclick=addProductToCart("+popularProductsList[i].id+")>";
		html += "<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' viewBox='0 0 24 24' fill='none'";
        html += "stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round'";
        html += "class='feather feather-plus'>";
        html += "<line x1='12' y1='5' x2='12' y2='19'></line>";
        html += "<line x1='5' y1='12' x2='19' y2='12'></line>";
        html += "</svg> Agregar al carro </a></div>";
        html += "<div class='d-flex justify-content-start text-center mt-3'>";
        html += "<div class='deals-countdown w-100' data-countdown='2028/10/10 00:00:00'></div>";
        html += "</div></div></div></div>";
	}
    
	jQuery("#temporalOfertsRow").html(html);
}