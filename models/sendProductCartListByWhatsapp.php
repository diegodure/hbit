<?php
// PHP (enviar_whatsapp.php)
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Recibe el mensaje del cuerpo de la solicitud POST
    $mensaje = isset($_POST['mensaje']) ? $_POST['mensaje'] : '';

    // Configura la URL y los datos de la solicitud para la API de WhatsApp Business
    $url = 'https://api.whatsapp.com/send?phone=+5991454527&text=' . urlencode($mensaje);

    // Realiza una redirección a la URL de WhatsApp para enviar el mensaje automáticamente
    header('Location: ' . $url);
    exit();
}
?>